<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Form</title>
    </head>

    <body>
        <h1>Buat Account Baru!</h1>
        <form action="/selamat" GET="POST">
        <h3>Sign Up Form</h3>

        <label>First name:</label> <br> <br>
        <input type="text" name="firstname"> <br> <br>
        <label>Last name:</label> <br> <br>
        <input type="text" name="lastname"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name="WN">
            <option value="WNI">Indonesian</option>
            <option value="WNA">US</option>
            <option value="WNA1">Other</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="radio" name="Indo"> Bahasa Indonesia <br>
        <input type="radio" name="English"> English <br>
        <input type="radio" name="Other"> Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea name="alamat" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="Sign Up"> 

    </form>
    </body>
</html>