<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form() {
        return view('form');
    }

    public function kirim(Request $request) {
        $firstname = $request->firstname;
        $lastname = $request->lastname;

        return view('selamat', compact('firstname','lastname'));
    }
}
